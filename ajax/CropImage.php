<?php


class CropImage
{
    const FROM_TOP = 0;
    const FROM_CENTER = 1;
    const FROM_BOTTOM = 2;

    private $image = null;
    private $image_folder = '';

    public function __construct($folder, $file_name, $format)
    {
        $file = $folder . $file_name . '.' . $format;
        $this->image_folder = $folder;
        switch ($format) {
            case 'jpg':
            case 'jpeg':
                $this->image = imagecreatefromjpeg($file);
                break;
            case 'png':
                $this->image = imagecreatefrompng($file);
                break;
            case 'bmp':
                $this->image = imagecreatefrombmp($file);
                break;
            case 'webp':
                $this->image = imagecreatefromwebp($file);
                break;
        }
    }

    public function getCroppedImages($out_format)
    {
        $image_width = imagesx($this->image);
        $image_height = imagesy($this->image);
        $part_width = floor($image_width / 5);
        $to_return = [];
        $img_num = 0;
        while ($img_num < 5) {
            $y_indent = 0;
            switch ($img_num) {
                case 0:
                case 4:
                    $y_indent = floor($image_height * 0.33);
                    break;
                case 1:
                case 3:
                    $y_indent = floor($image_height * 0.165);
                    break;
            }
            $cropped_image = imagecrop($this->image,
                [
                    'x' => $part_width * $img_num,
                    'y' => $y_indent,
                    'width' => $part_width,
                    'height' => $image_height - $y_indent * 2
                ]);
            if ($cropped_image) {
                $folder = $this->image_folder . 'cropped_image' . $img_num;
                switch ($out_format) {
                    case 'jpg':
                    case 'jpeg':
                        imagejpeg($cropped_image, $this->image_folder . 'cropped_image' . $img_num . '.' . $out_format);
                        break;
                    case 'png':
                        imagepng($cropped_image, $this->image_folder . 'cropped_image' . $img_num . '.' . $out_format);
                        break;
                    case 'bmp':
                        imagebmp($cropped_image, $this->image_folder . 'cropped_image' . $img_num . '.' . $out_format);
                        break;
                    case 'webp':
                        imagewebp($cropped_image, $this->image_folder . 'cropped_image' . $img_num . '.' . $out_format);
                        break;
                }
                array_push($to_return, 'cropped_image' . $img_num . '.' . $out_format);
            }
            $img_num++;
        }
        return $to_return;
    }
}
