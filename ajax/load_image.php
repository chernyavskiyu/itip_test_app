<?php
require_once('CropImage.php');

if ($_FILES['image']['size'] > (30 * 1024 * 1024)){
    die('Max size 30MB');
}
if (!stristr($_FILES['image']['type'], 'image')){
    die('Incorrect file format');
}

$file_name_data = explode('.', $_FILES['image']['name']);
$format = end($file_name_data);
$file_name = 'original_image';
$image_folder = $_SERVER['DOCUMENT_ROOT'] . '/itip/img/';
move_uploaded_file($_FILES['image']['tmp_name'], $image_folder . $file_name . '.' . $format);

$crop = new CropImage($image_folder, $file_name, $format);
echo json_encode($crop->getCroppedImages('png'));

?>
