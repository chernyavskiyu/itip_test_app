<?php

?>
<html lang="en">
<head>
    <title>itip test application</title>
    <link rel="stylesheet" href="/itip/styles/style.css">
</head>
<body>
<div class="container">

    <hr>
    <div>
        <label for="">
            Select your file
            <input type="file" name="image_input" accept=".jpg,.jpeg,.png,.bmp,.webp">
        </label>
        <button class="btn">Загрузить</button>

        <div class="progress-bar" style="display: none">
            <div class="progress">0%</div>
        </div>
    </div>
    <hr>
    <div class="content" style="display: none">
        <div class="loader">
            <div class="loader-text">Обработка.</div>
        </div>
        <img src="" alt="test-image" class="test-image">
    </div>
    <div class="result">

    </div>
</div>
</body>
<script src="/itip/js/jquery.js"></script>
<script>
    $(document).ready(function () {
        $('.btn').click(function () {
            let input = $('input[name="image_input"]')[0];
            if (input.files && input.files[0]) {
                if (input.files[0].size > (30 * 1024 * 1024)) {
                    alert('Max size 30MB');
                    return;
                }
                let data = new FormData();
                data.append('image', input.files[0]);
                $.ajax({
                    type: 'POST',
                    url: "/itip/ajax/load_image.php",
                    data: data,
                    dataType: 'text',
                    cache: false,
                    contentType: false,
                    processData: false,
                    xhr: function () {
                        let xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (e) {
                            if (e.lengthComputable) {
                                let percent = (e.loaded / e.total) * 100;
                                $('.progress-bar > .progress').width(percent + '%').text(Math.trunc(percent) + '%');
                                if (Math.trunc(percent) == 100) {
                                    $('.progress-bar').toggle();

                                    let reader = new FileReader();
                                    reader.onload = function (event) {
                                        $('.test-image').attr('src', event.target.result);
                                    };
                                    reader.readAsDataURL(input.files[0]);
                                    $('.content').toggle();
                                    loaderDots('.loader-text', 500);
                                }
                            }
                        }, false);
                        return xhr;
                    },
                    beforeSend: function () {
                        $('.progress-bar').toggle();
                    },
                    success: function (data) {
                        $('.content').toggle();
                        let images = JSON.parse(data);
                        for (let num in images) {
                            $('.result').append($(document.createElement('img')).prop('src', '/itip/img/' + images[num]));
                        }
                    }
                });
            }


        });

        function loaderDots(elemQuery, interval) {
            let dots = 1;
            setInterval(function () {
                $(elemQuery).text('Обработка' + '.'.repeat(dots));
                if (dots == 3) {
                    dots = 1;
                } else {
                    dots++;
                }
            }, interval);
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    });
</script>
<script src="/itip/js/script.js"></script>
</html>
